import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;

public class PracticalTaskN1 {
    public static void main(String[] args) throws FileNotFoundException {
        System.setIn(new FileInputStream(new File("input.txt")));
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            Map<String, Integer> word = WordCounter.countWord(sc.nextLine());

            System.out.println("Selected word appeared " + word.get("hello") + " times");
        }
    }
}