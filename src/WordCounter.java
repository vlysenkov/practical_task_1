import java.util.HashMap;
import java.util.Map;

public class WordCounter {
    public static Map<String, Integer> countWord(String word) {

        String text = word.toLowerCase().replaceAll("[^a-zA-Z ]", "");
        String[] splitted = text.split(" ");
        System.out.println("Word line's count = " + splitted.length);

        Map<String, Integer> allWords = new HashMap<String, Integer>();
        for (int i = 0; i < splitted.length; i++) {
            if (allWords.containsKey(splitted[i])) {
                allWords.put(splitted[i], allWords.get(splitted[i]) + 1);
            } else {
                allWords.put(splitted[i], 1);
            }
        }
        return allWords;
    }
}