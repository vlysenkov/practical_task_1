import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.google.common.truth.Truth.assertThat;

class WordCounterTest {

    @Test
    void testLine1() {
        String test = "Hello, my hello hello friend.";
        Map<String, Integer> actual = WordCounter.countWord(test);
        assertThat(actual)
                .containsExactly("hello", 3, "my", 1, "friend", 1);
    }

    @Test
    void testLine2() {
        String test = "hello am here, hello we go to";
        Map<String, Integer> actual = WordCounter.countWord(test);
        assertThat(actual)
                .containsExactly("hello", 2, "here", 1, "go", 1, "to", 1, "am", 1, "we", 1);
    }

    @Test
    void testLine3() {
        String test = "Sandra’s place my";
        Map<String, Integer> actual = WordCounter.countWord(test);
        assertThat(actual)
                .containsExactly("sandras", 1, "place", 1, "my", 1);
    }
}

